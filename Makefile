fat:main.o stack.o sys.o file.o disk.o dir.o
	gcc main.o stack.o sys.o file.o disk.o dir.o -o fat
main.o:main.c
	gcc -c main.c -o main.o
stack.o:stack.c
	gcc -c stack.c -o stack.o
sys.o:sys.c
	gcc -c sys.c -o sys.o
file.o:file.c
	gcc -c file.c -o file.o
disk.o:disk.c
	gcc -c disk.c -o disk.o
dir.o:dir.c
	gcc -c dir.c -o dir.o
.PHONY:clean
clean:
	rm -rf *.o

